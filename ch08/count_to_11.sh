#!/bin/bash

# Count to 11 in 10 different ways.

n=1;
echo -n "$n "

let "n = $n + 1"    # let "n = n + 1" also works.
echo -n "$n "

: $(( n = $n + 1 ))
echo -n "$n "

(( n = n + 1 ))
echo -n "$n "

n=$(($n + 1))
echo -n "$n "

: $[ n = $n + 1 ]
echo -n "$n "

n=$[ $n + 1 ]
# Avoid this type of construct, since it is obsolete and nonportable.
echo -n "$n "


# C-style increment operators.

let "n++"
echo -n "$n "

(( n++ ))
echo -n "$n "

: $(( n++ ))
echo -n "$n "

: $[ n++ ]
echo -n "$n "

echo

exit 0

