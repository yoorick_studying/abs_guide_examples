#!/bin/bash

# Manipulating a variable, C-style, using the (( ... )) construct.


echo

(( a = 23 ))
echo "a (initial value) = $a"	# 23

(( a++ ))
echo "a (after a++) = $a"		# 24

(( a-- ))
echo "a (after a--) = $a"		# 23

(( ++a ))
echo "a (after ++a) = $a"		# 24

(( --a ))
echo "a (after --a) = $a"		# 23

echo


n=1; let --n && echo "true" || echo "false"
n=1; let n-- && echo "true" || echo "false"

echo


(( t = a<45?7:11 ))
echo "t = $t"			# a = 23; t = 7.

echo

exit 0

