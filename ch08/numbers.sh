#!/bin/bash

# Representation of numbers in different bases.

# Decimal: the default
let "dec = 32"
echo "decimal number = $dec"		# 32

# Octal: numbers preceded by '0'
let "oct = 032"
echo "octal number = $oct"			# 26

# Hexadecimal: numbers preceded by '0x' or '0X'
let "hex = 0x32"
echo "hexadecimal number= $hex"		# 50

echo $(( 0x9abc ))					# 39612
echo $(( 0x9ABC ))


# Other bases: BASE#NUMBER
# BASE between 2 and 64

let "bin = 2#111100111001101"
echo "binary number = $bin"			# 31181

let "b32 = 32#77"
echo "base-32 number = $b32"		# 231

let "b64 = 64#@_"
echo "base-64 number = $b64"		# 4031


echo

echo $(( 36#zz )) $(( 2#10101010 )) $(( 16#AF16 )) $(( 53#1aA ))
									# 1295 170 44822 3375

let "bad_oct = 081"

exit $?		# Exit value = 1 (error)

