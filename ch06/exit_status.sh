#!/bin/bash

echo hello
echo $?

lskdf
echo $?

echo

true
echo "exit status of \"true\" = $?"

! true
echo "exit status of \"! true\" = $?"

true
!true
echo $?

echo

# Pipe

ls | bogus_command
echo $?

! ls | bogus_command
echo $?


exit 113

