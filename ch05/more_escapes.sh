#!/bin/bash

echo "Hello"
echo "\"Hello\" ... he said."

echo "\$variable01"
echo "The book cost \$7.98."

echo "\\"
echo '\'

echo

echo \z
echo \\z
echo '\z'
echo '\\z'
echo "\z"
echo "\\z"

echo

# Command substitution
echo `echo \z`
echo `echo \\z`
echo `echo \\\z`
echo `echo \\\\z`
echo `echo \\\\\\z`
echo `echo \\\\\\\z`
echo `echo "\z"`
echo `echo "\\z"`

echo

# Here document

cat <<EOF
\z
EOF

cat <<EOF
\\z
EOF

echo

# Variables

variable=\
echo "$variable"
# Will not work

variable=\
23skidoo
echo "$variable"

variable=\\
echo "$variable"

variable=\\\
echo "$variable"
# Will not work

variable=\\\\
echo "$variable"

echo

file_list="/bin/cat /bin/gzip /bin/more /usr/bin/less"

ls -l /bin/bash /sbin/dump $file_list

ls -l /bin/bash\ /sbin/dump\ $file_list

echo

echo "foo
bar"

echo 'foo
bar'

echo foo\
bar

echo "foo\
bar"

echo 'foo\
bar'


exit 0

