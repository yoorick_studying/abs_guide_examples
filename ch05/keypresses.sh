#!/bin/bash

key="no value yet"

while true; do
	clear
	echo "Bash Extra Keys Demo. Keys to try:"
	echo
	echo "* Insert, Delete, Home, End, Page_Up and Page_Down"
	echo "* The four arrow keys"
	echo "* Tab, enter, escape, and space key"
	echo "* The letter and number keys, etc."
	echo
	echo "  d = show date/time"
	echo "  q = quit"
	echo "=================================================="
	echo

	# !!! Home and End keys do not work on my computer
	# (They are printed in "*)" part of the case statement)
	# TODO: Fix it later
	
	# Convert the separate home-key to home-key_num_7:
	if [ "$key" = $'\x1b\x4f\x48' ]; then
		key=$'\x1b\x5b\x31\x7e'
	fi

	# Convert the separate end-key to end-key_num_1:
	if [ "$key" = $'\x1b\x4f\x46' ]; then
		key=$'\x1b\x5b\x34\x7e'
	fi

	case "$key" in
		$'\x1b\x5b\x32\x7e')    # Insert
			echo Insert key
		;;
		$'\x1b\x5b\x33\x7e')    # Delete
			echo Delete key
		;;
		$'\x1b\x5b\x31\x7e')    # Home
			echo Home key
		;;
		$'\x1b\x5b\x34\x7e')    # End
			echo End key
		;;
		$'\x1b\x5b\x35\x7e')    # Page_Up
			echo Page_Up key
		;;
		$'\x1b\x5b\x36\x7e')    # Page_Down
			echo Page_Down key
		;;
		$'\x1b\x5b\x41')    # Up_arrow
			echo Up arrow
		;;
		$'\x1b\x5b\x42')    # Down_arrow
			echo Down arrow
		;;
		$'\x1b\x5b\x43')    # Right_arrow
			echo Right arrow
		;;
		$'\x1b\x5b\x44')    # Left_arrow
			echo Left arrow
		;;
		$'\x09')    # Tab
			echo Tab key
		;;
		$'\x0a')    # Enter
			echo Enter key
		;;
		$'\x1b')    # Escape
			echo Escape key
		;;
		$'\x20')    # Space
			echo Space key
		;;
		d)
			date
		;;
		q)
			echo Time to quit...
			echo
			exit 0
		;;
		*)
			echo You pressed: \'"$key"\'
		;;
	esac

	echo
	echo "=================================================="

	unset K1 K2 K3
	read -s -N1 -p "Press a key: "
	K1="$REPLY"
	read -s -N2 -t 0.001
	K2="$REPLY"
	read -s -N1 -t 0.001
	K3="$REPLY"
	key="$K1$K2$K3"

done

exit $?

