#!/bin/bash

# Quoting variables

List="one two three"

for a in $List
do
	echo "$a"
done

echo "---"

for a in "$List"
do
	echo "$a"
done

echo

variable1="a variable containing five words"
echo This is $variable1
# 7 arguments
echo "This is $variable1"
# 1 argument

variable2=""    # Empty

echo $variable2 $variable2 $variable2
# No arguments
echo "$variable2" "$variable2" "$variable2"
# 3 empty arguments
echo "$variable2 $variable2 $variable2"
# 1 argument (2 spaces)

# Weird variables

var="'(]\\{}\$\""
echo $var
echo "$var"

OLD_IFS="$IFS"
IFS='\'
echo $var
echo "$var"
#IFS="$OLD_IFS"

echo

var2="\\\\\""
echo $var2
echo "$var2"

echo

var3='\\\\'
echo "$var3"

IFS="$OLD_IFS"

echo

echo "Why can't I write 's between single quotes"

echo 'Why can'\''t I write '"'"'s between single quotes'

exit 0

