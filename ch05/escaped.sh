#!/bin/bash
# Escaped characters

# Escaping a newline

echo ""

echo "This will print
as two lines."

echo "This will print \
as one line."

echo; echo

echo "=============="

echo "\v\v\v\v"
echo "=============="
echo "VERTICAL TABS"
echo -e "\v\v\v\v"
echo "=============="

echo "QUOTATION MARK"
echo -e "\042"
echo "=============="

echo

# The $'\X' construct

echo "NEWLINE"
echo $'\n'

# The $'\NNN' string expansion

echo $'\t \042 \t'   # Quote (") framed by tabs
echo $'\t \x22 \t'   # Quote (") framed by tabs

echo

quote=$'\042'
echo "$quote Quoted string $quote and this lies outside the quotes."

echo

triple_underline=$'\137\137\137'   # 137 is octal ASCII code for '_'.
echo "$triple_underline UNDERLINE $triple_underline"

echo

ABC=$'\101\102\103\010'    # 101, 102, 103 are octal A, B, C.
echo $ABC

echo

escape=$'\033'   # 033 is octal for escape.
echo "\"escape\" echoes as ${escape}"
#                                  no visible output

echo

exit 0

