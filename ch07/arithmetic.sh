#!/bin/bash

# Arithmetic tests.

# The (( ... )) construct evalueates and tests numerical expressions.
# Exit status opposite from [ ... ] construct !

echo

(( 0 ))
echo "Exit status of \"(( 0 ))\" is $?."   # 1

(( 1 ))
echo "Exit status of \"(( 1 ))\" is $?."   # 0

(( 5 > 4 ))                                    # true
echo "Exit status of \"(( 5 > 4 ))\" is $?."   # 0

(( 5 > 9 ))                                    # false
echo "Exit status of \"(( 5 > 9 ))\" is $?."   # 1

(( 5 == 5 ))                                   # true
echo "Exit status of \"(( 5 == 5 ))\" is $?."  # 0

(( 5 - 5 ))                                    # 0
echo "Exit status of \"(( 5 - 5 ))\" is $?."   # 1

(( 5 / 4 ))                                    # Division o.k.
echo "Exit status of \"(( 5 / 4 ))\" is $?."   # 0

(( 1 / 2 ))                                    # Rounded off to 0.
echo "Exit status of \"(( 1 / 2 ))\" is $?."   # 1

(( 1 / 0 )) 2>/dev/null                        # Error!
echo "Exit status of \"(( 1 / 0 ))\" is $?."   # 1


var1=5
var2=4

if (( var1 > var2 ))
then
	echo "$var1 is greater than $var2."
fi

echo

(( 0 && 1 ))   # Logical AND
echo $?        # 1
let "num = (( 0 && 1 ))"
echo $num      # 0
let "num = (( 0 && 1 ))"
echo $?        # 1

echo

(( 200 || 11 ))    # Logical OR
echo $?            # 0
let "num = (( 200 || 11 ))"
echo $num          # 1
let "num = (( 200 || 11 ))"
echo $?            # 0

echo

(( 200 | 11 ))     # Bitwise OR
echo $?            # 0
let "num = (( 200 | 11 ))"
echo $num          # 203
let "num = (( 200 | 11 ))"
echo $?            # 0

echo

var=-2 && (( var += 2 ))
echo $?

var=-2 && (( var += 2 )) && echo $var     # Will not echo $var!

echo

var1=017
var2=0x0f

var3=$(( var1 + var2 ))
echo "\$var3 = $var3"

echo

exit 0

