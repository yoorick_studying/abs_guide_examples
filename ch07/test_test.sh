#!/bin/bash

echo

# Check type of the commands

type test

type '['

type '[['

type ']]'

type ']'


echo

# Equivalence of test, /usr/bin/test, [] and /usr/bin/[

if test -z "$1"
then
	echo "No command-line arguments."
else
	echo "First command-line argument is $1."
fi

if /usr/bin/test -z "$1"
then
	echo "No command-line arguments."
else
	echo "First command-line argument is $1."
fi

if [ -z "$1" ]
then
	echo "No command-line arguments."
else
	echo "First command-line argument is $1."
fi

if /usr/bin/[ -z "$1" ]
then
	echo "No command-line arguments."
else
	echo "First command-line argument is $1."
fi


echo

var1=20
var2=22
[ "$var1" -ne "$var2" ] && echo "$var1 is not equal to $var2"

home=/home/bozo
[ -d "$home" ] || echo "$home directory does not exist."


echo

# Test [[ ]]

file=/etc/passwd

if [[ -e $file ]]
then
	echo "Password file exists."
fi

echo

# Octal and hexadecimal evaluation

decimal=15
octal=017
hex=0x0f

echo "decimal=$decimal"
echo "octal=$octal"
echo "hex=$hex"

# Doesn't evaluate within [ single brackets ] !
if [ "$decimal" -eq "$octal" ]
then
	echo "$decimal equals $octal."
else
	echo "$decimal is not equal to $octal."
fi

# Evaluates within [[ double brackets ]] !
if [[ "$decimal" -eq "$octal" ]]
then
	echo "$decimal equals $octal."
else
	echo "$decimal is not equal to $octal."
fi

# Also evaluates
if [[ "$decimal" -eq "$hex" ]]
then
	echo "$decimal equals $hex."
else
	echo "$decimal is not equal to $hex."
fi

echo

exit 0

