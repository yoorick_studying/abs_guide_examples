#!/bin/bash

# echo 'Hello, world!' | md5sum > a
# echo 'Hello, world!' | md5sum > b
# echo 'Hello, extraterrestrial people!' | md5sum > c

echo

# Make the paths relative to where the script is placed and not to current working directory
base_path=`dirname $0`
a="$base_path/a"
b="$base_path/b"
c="$base_path/c"

#if cmp a b &> /dev/null
if cmp "$a" "$b" &> /dev/null
	then echo "Files a and b are identical."
	else echo "Files a and b differ."
fi

#if cmp a c &> /dev/null
if cmp "$a" "$c" &> /dev/null
	then echo "Files a and c are identical."
	else echo "Files a and c differ."
fi

if grep -q Bash "$base_path/bash.txt"
	then echo "File contains at least one occurrence of Bash."
fi

word=Linux
letter_sequence=inu
if echo "$word" | grep -q "$letter_sequence"
then
	echo "\"$letter_sequence\" found in \"$word\""
else
	echo "\"$letter_sequence\" not found in \"$word\""
fi

echo

if [ -d "$base_path/bash.txt" ]
then
	echo "\"bash.txt\" is a directory."
else
if [ -f "$base_path/bash.txt" ]
then
	echo "\"bash.txt\" is a regular file."
else
	echo "\"bash.txt\" doesn't exist or is something other then a directory or a regular file."
fi
fi

if [ -d "$base_path/bash.txt" ]
then
	echo "\"bash.txt\" is a directory."
elif [ -f "$base_path/bash.txt" ]
then
	echo "\"bash.txt\" is a regular file."
else
	echo "\"bash.txt\" doesn't exist or is something other then a directory or a regular file."
fi

echo

var=3

if [ "$var" -gt 0 ]
then
	if [ "$var" -lt 5 ]
	then
		echo "The value of \"var\" lies somewhere between 0 and 5."
	fi
fi

# Same result as:
if [ "$var" -gt 0 ] && [ "$var" -lt 5 ]
then
	echo "The value of \"var\" lies somewhere between 0 and 5."
fi

echo

exit 0

