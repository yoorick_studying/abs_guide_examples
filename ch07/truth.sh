#!/bin/bash

# What is truth?

echo

echo "Testing \"0\""
if [ 0 ]
then
	echo "0 is true."
else
	echo "0 is false."
fi
# 0 is true

echo

echo "Testing \"1\""
if [ 1 ]
then
	echo "1 is true."
else
	echo "1 is false."
fi
# 1 is true

echo

echo "Testing \"-1\""
if [ -1 ]
then
	echo "-1 is true."
else
	echo "-1 is false."
fi
# -1 is true

echo

echo "Testing \"NULL\""
if [ ]
then
	echo "NULL is true."
else
	echo "NULL is false."
fi
# NULL is false

echo

echo "Testing \"xyz\""
if [ xyz ]
then
	echo "Random string is true."
else
	echo "Random string is false."
fi
# Random string is true

echo

echo "Testing \"\$xyz\""
if [ $xyz ]     # Uninitialized variable
then
	echo "Uninitialized variable is true."
else
	echo "Uninitialized variable is false."
fi
# Uninitialized variable is false.

echo

echo "Testing \"-n \$xyz\""
if [ -n "$xyz" ]   # More pedantically correct
then
	echo "Uninitialized variable is true."
else
	echo "Uninitialized variable is false."
fi
# Uninitialized variable is false

echo

xyz=            # Initialized, but set to null value

echo "Testing \"-n \$xyz\""
if [ -n "$xyz" ]
then
	echo "Null variable is true."
else
	echo "Null variable is false."
fi
# Null variable is false

echo

# When is "false" true?

echo "Testing \"false\""
if [ "false" ]     # "false" is just a string
then
	echo "\"false\" is true."
else
	echo "\"false\" is false."
fi
# "false" is true

echo

echo "Testing \"\$false\""
if [ "$false" ]    # Again, uninitialized variable
then
	echo "\"\$false\" is true."
else
	echo "\"\$false\" is false."
fi
# "$false" is false

echo

echo "Testing \"false\""
if [ false ]       # false is treated like a string, not a command)
then
	echo "\"false\" is true."
else
	echo "\"false\" is false."
fi
# "false" is true

echo

echo "Testing \"\`false\`\""
if [ `false` ]    # `false` doesn't retun any output, so it is equivalent to "if [ ]".
                    # Exit status doesn't matter in this case
then
	echo "\`false\` is true."
else
	echo "\`false\` is false."
fi
# `false` is false

echo

# When is "true" true?

echo "Testing \"\$true\""
if [ "$true" ]     # Uninitialized variable
then
	echo "\"\$true\" is true."
else
	echo "\"\$true\" is false"
fi
# "$true" is false

echo

echo "Testing \"\`true\`\""
if [ `true` ]      # And again, `true` doesn't return any output and its exit status is not checked.
then
	echo "\`true\` is true."
else
	echo "\`true\` is false"
fi
# `true` is false

echo

exit 0

