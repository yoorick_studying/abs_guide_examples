#!/bin/bash
# Basename check

# Requires symbolic links:
# ln -s ./ex18.sh ./wh-tucows
# ln -s ./ex18.sh ./wh-ripe
# ln -s ./ex18.sh ./wh-apnic
# ln -s ./ex18.sh ./wh-cw

E_NOARGS=75

if [ -z "$1" ]
then
	echo "Usage: `basename $0` [domain-name]"
	exit $E_NOARGS
fi

# Check script name and call proper server.
case `basename $0` in
	"ex18.sh"   ) whois $1;;
	"wh-tucows" ) whois -h whois.tucows.com $1;;
	"wh-ripe"   ) whois -h whois.ripe.net $1;;
	"wh-apnic"  ) whois -h whois.apnic.net $1;;
	"wh-cw"     ) whois -h whois.cw.net $1;;
	*           ) whois $1;;
esac

exit $?

