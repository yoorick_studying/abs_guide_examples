#!/bin/bash

# Variable Assignment

# Naked variables

echo

a=879
echo "The value of \"a\" is $a."

let a=16+5
echo "The value of \"a\" is now $a."

echo

for a in 7 8 9 11
do
	echo -n "$a "
done

echo
echo

#echo -n "Enter \"a\" "
#read a
#echo "The value of \"a\" is now $a."

#echo

# Now, getting a little bit fancier (command subsctitution).

a=23
echo $a
b=$a
echo $b

a=`echo Hello!`
echo $a

a=`ls -l`
echo $a
echo
echo "$a"

arch=$(uname -m)
echo "$arch"

echo
echo

str=''
for i in one two three
do
	str="${str}${i}"$'\n'
done

echo $str
echo "$str"

echo

# Закомментировал, т.к. много строк вывода генерировали
#for i in "$a"
#do
#	echo "!!!"
#	echo "$i"
#	echo "!!!"
#done

#for i in $a
#do
#	echo "$i"
#done

#echo "!!!"

a="one two three 1 2 3 a b c"
b="\"one two three\" \"1 2 3\" \"a b c\""
for i in "$a"
do
	echo "$i"
done

for i in $a
do
	echo "$i"
done

for i in $b
do
	echo "$i"
done

echo

exit 0

