#!/bin/bash

a=2334
echo "a = $a"
let "a += 1"
echo "a = $a"
echo

b=${a/23/BB}

echo "b = $b"
declare -i b
echo "b = $b"

let "b += 1"
echo "b = $b"
echo

c=BB34
echo "c = $c"

d=${c/BB/23}

echo "d = $d"
let "d += 1"
echo "d = $d"
echo

# What about null variables?
e=''
echo "e = $e"
let "e += 1"
echo "e = $e"
echo

# What about undeclared variables?
echo "f = $f"
let "f += 1"
echo "f = $f"
echo

# However...
let "f /= $undecl_var"

# But still...
let "f /= 0"

exit $?

